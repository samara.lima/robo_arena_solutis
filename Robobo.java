package robo.arena.solutis;
import robocode.*;
import java.awt.Color;


//Robobo - a robot by (Samara lima)
 
public class Robobo extends Robot {

	public void run() {

		 //alterando as cores:
		 setBodyColor(Color.cyan);
		 setGunColor(Color.red);
		 setRadarColor(Color.black);
		 setScanColor(Color.orange);
		 setBulletColor(Color.red);
		 
		while(true) {
			ahead(100);
			turnRight(90);
			ahead(100);
			turnGunRight(360);
			ahead(100);
			turnRight(90);
			ahead(100);
			turnGunRight(360);
}
	}
	
// se o radar encontrar o robo, ele atira, c a potencia (fire)
	public void onScannedRobot(ScannedRobotEvent e) { 
		if (e.getDistance() < 50 && getEnergy() > 50) { //se inimigo está proximo e meu robo tenho mt vida (energia) 
			fire(5); //atira c mt potencia
			
		} else {  // caso contrario, atira c pouca potencia
			fire(3);
}		
		scan(); //
	}
	
	public void onHitByBullet(HitByBulletEvent e) {
 //se o robo for atingido por um projétil
		back(e.getPower());// recua e aumenta o poder de fogo
		fire(3);
		
	}
	
	public void onHitWall(HitWallEvent e) { 
// se o robo bater na parede, anda p trás
		back(20);
		 
	}	

public void onHitRobot(HitRobotEvent e){ // quando houver colisão c robo inimigo
		turnRight(e.getBearing()); //getBearing() retorna o ângulo do robô adversário em relação ao seu robô
			fire(3);
			turnGunRight(360);
}
}






